import { RouteProp } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import * as React from 'react';
import { StyleSheet } from 'react-native';
import { FlatList, View } from 'react-native';
import {
  List,
  Avatar,
  TouchableRipple,
  Divider,
  Searchbar,
  FAB,
  Dialog,
  Portal,
  Paragraph,
  Button,
  TextInput
} from 'react-native-paper';
import { RootStackParamList } from '../App';
import { useContacts } from '../hooks/useContacts';

type ContactListRouteProp = RouteProp<RootStackParamList, 'ContactList'>;

type ContactListNavigationProp = StackNavigationProp<RootStackParamList, 'ContactList'>;

type Props = {
  navigation: ContactListNavigationProp;
  route: ContactListRouteProp;
};

export const ContactList = ({ navigation, route }: Props) => {
  const [contacts, addContact, deleteContact] = useContacts();
  const [search, setSearch] = React.useState('');
  const [visible, setVisible] = React.useState(false);

  if (route?.params?.contactToDelete) {
    deleteContact(route.params.contactToDelete);
  }

  const [name, setName] = React.useState('');
  const [phone, setPhone] = React.useState('');

  const filterByName = (name: string, query: string) =>
    name.toLowerCase().indexOf(query.toLowerCase()) !== -1;

  const showDialog = () => setVisible(true);
  const hideDialog = () => setVisible(false);

  const onAddContact = () => {
    if (name === '' || phone === '') {
      return;
    }

    addContact(name, phone);
  };

  return (
    <View style={styles.container}>
      <Searchbar value={search} onChangeText={text => setSearch(text)} />
      <FlatList
        ItemSeparatorComponent={() => <Divider />}
        data={contacts.filter(c => filterByName(c.name, search))}
        renderItem={({ item }) => {
          const { id, name, emails, imageAvailable, image, phoneNumbers } = item;

          const email = emails && emails.length > 0 ? emails[0].email : '';
          const imageUri = imageAvailable && image ? image.uri : undefined;
          const avatar = imageUri ? (
            <Avatar.Image size={48} source={{ uri: imageUri }} />
          ) : (
            <Avatar.Text size={48} label={name[0]} />
          );

          return (
            <TouchableRipple
              onPress={() =>
                navigation.navigate('ContactDetails', {
                  id,
                  name,
                  numbers: phoneNumbers,
                  image: imageUri
                })
              }
              rippleColor="rgba(0, 0, 0, .32)"
            >
              <List.Item title={name} description={email} left={() => avatar} />
            </TouchableRipple>
          );
        }}
      />
      <FAB style={styles.fab} icon="plus" onPress={showDialog} />
      <Portal>
        <Dialog visible={visible} onDismiss={hideDialog}>
          <Dialog.Title>Add new contact</Dialog.Title>
          <Dialog.Content>
            <TextInput
              style={styles.action}
              label="Name"
              value={name}
              onChangeText={text => setName(text)}
            />
            <TextInput label="Phone" value={phone} onChangeText={text => setPhone(text)} />
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={onAddContact}>Confirm</Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginBottom: 48
  },
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0
  },
  action: {
    marginBottom: 8
  }
});
