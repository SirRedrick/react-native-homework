import { RouteProp } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import * as React from 'react';
import * as Linking from 'expo-linking';
import { StyleSheet } from 'react-native';
import { View } from 'react-native';
import { Avatar, Button, Subheading, Text, Title } from 'react-native-paper';
import { RootStackParamList } from '../App';

type ContactRouteProp = RouteProp<RootStackParamList, 'ContactDetails'>;

type ContactNavigationProp = StackNavigationProp<RootStackParamList, 'ContactDetails'>;

type Props = {
  route: ContactRouteProp;
  navigation: ContactNavigationProp;
};

export const ContactDetails = ({ route, navigation }: Props) => {
  const { id, name, numbers, image } = route.params;
  const imageSize = 128;

  const avatar = image ? (
    <Avatar.Image style={styles.avatar} size={imageSize} source={{ uri: image }} />
  ) : (
    <Avatar.Text style={styles.avatar} size={imageSize} label={name[0]} />
  );

  return (
    <View style={styles.container}>
      {avatar}

      <View style={styles.item}>
        <Subheading>Name</Subheading>
        <Title>{name}</Title>
      </View>
      {numbers && (
        <View style={styles.item}>
          <Subheading>Phone number</Subheading>
          <Title>{numbers[0].number}</Title>
        </View>
      )}

      <View style={styles.actions}>
        <Button
          disabled={!numbers}
          onPress={() => {
            if (!numbers) return;
            Linking.openURL(`tel:${numbers[0].number}`);
          }}
          style={styles.action}
          mode="contained"
        >
          Call
        </Button>
        <Button
          style={styles.action}
          mode="contained"
          onPress={() => navigation.navigate('ContactList', { contactToDelete: id })}
        >
          Delete
        </Button>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: 32,
    paddingHorizontal: 16
  },
  avatar: {
    elevation: 4,
    alignSelf: 'center',
    marginBottom: 32
  },
  item: {
    borderWidth: 1,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderLeftWidth: 0,
    borderBottomColor: '#999',
    marginBottom: 16
  },
  actions: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginTop: 24
  },
  action: {
    flex: 1,
    margin: 8
  }
});
