import { StackHeaderProps } from '@react-navigation/stack';
import * as React from 'react';
import { Appbar } from 'react-native-paper';

export const Navigation = ({ navigation, previous }: StackHeaderProps) => (
  <Appbar.Header>
    {previous ? <Appbar.BackAction onPress={navigation.goBack} /> : null}
    <Appbar.Content title="Contacts" />
  </Appbar.Header>
);
