import * as Contacts from 'expo-contacts';
import 'react-native-get-random-values';
import uuid from 'react-native-uuid';
import { useState, useEffect } from 'react';

export const useContacts = () => {
  const [contacts, setContacts] = useState<Contacts.Contact[]>([]);

  const addContact = async (name: string, phone: string) => {
    const contact = {
      id: String(uuid.v4()),
      name,
      phoneNumber: phone,
      contactType: 'person'
    };
    const contactId = await Contacts.addContactAsync(contact);
    const newContact = await Contacts.getContactByIdAsync(contactId);

    if (newContact) {
      const newContacts = [...contacts, newContact];
      setContacts(newContacts);
    }
  };

  const deleteContact = (id: string) => {
    Contacts.removeContactAsync(id).then(() => {
      setContacts(contacts.filter(c => c.id !== id));
    });
  };

  useEffect(() => {
    (async () => {
      const { status } = await Contacts.requestPermissionsAsync();
      if (status === 'granted') {
        const { data } = await Contacts.getContactsAsync({
          fields: [Contacts.Fields.Emails, Contacts.Fields.Image, Contacts.Fields.PhoneNumbers]
        });

        setContacts(data);
      }
    })();
  }, []);

  return [contacts, addContact, deleteContact] as const;
};
