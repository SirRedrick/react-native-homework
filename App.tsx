import 'react-native-gesture-handler';
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { StatusBar } from 'expo-status-bar';
import { Provider as PaperProvider } from 'react-native-paper';
import { ContactList } from './components/ContactList';
import { Navigation } from './components/Navigation';
import { ContactDetails } from './components/ContactDetails';
import { PhoneNumber } from 'expo-contacts';

export type RootStackParamList = {
  ContactList: { contactToDelete: string };
  ContactDetails: {
    id: string;
    name: string;
    numbers: PhoneNumber[] | undefined;
    image: string | undefined;
  };
};

const RootStack = createStackNavigator<RootStackParamList>();

const App = () => {
  return (
    <NavigationContainer>
      <StatusBar style="light" />
      <PaperProvider>
        <RootStack.Navigator
          initialRouteName="ContactList"
          screenOptions={{
            header: props => <Navigation {...props} />
          }}
        >
          <RootStack.Screen name="ContactList" component={ContactList} />
          <RootStack.Screen name="ContactDetails" component={ContactDetails} />
        </RootStack.Navigator>
      </PaperProvider>
    </NavigationContainer>
  );
};

export default App;
